package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
)

func output(v interface{}) {
	data, _ := json.MarshalIndent(v, "", "\t")
	println(string(data))
}

type Element struct {
	H1      string
	H2      string
	Body    string
	Samples map[string]string
}

type Document struct {
	Elements []Element
}

type ElementType int

const (
	H1 ElementType = iota
	H2
	Body
	Samples
)

func (e Element) elmType() ElementType {
	if e.H1 != "" {
		return H1
	} else if e.H2 != "" {
		return H2
	} else if e.Body != "" {
		return Body
	} else if e.Samples != nil {
		return Samples
	}
	return Samples
}

func parse(data string) Document {
	ret := Document{}
	var currentBody string
	currentSamples := make(map[string]string)
	currentlySampling := false
	currentlySamplingName := ""
	currentlySamplingBody := ""
	for _, line := range strings.Split(strings.TrimSuffix(data, "\n"), "\n") {
		if currentlySampling {
			goto sampling
		}
		if strings.HasPrefix(line, "# ") {
			if currentBody != "" {
				ret.Elements = append(ret.Elements, Element{Body: currentBody})
				currentBody = ""
			}
			ret.Elements = append(ret.Elements, Element{H1: strings.TrimPrefix(line, "# ")})
			continue
		}
		if strings.HasPrefix(line, "## ") {
			if currentBody != "" {
				ret.Elements = append(ret.Elements, Element{Body: currentBody})
				currentBody = ""
			}
			ret.Elements = append(ret.Elements, Element{H2: strings.TrimPrefix(line, "## ")})
			continue
		}
		if strings.HasPrefix(line, ">>") && !strings.HasPrefix(line, ">>> ") {
			currentlySampling = true
			continue
		}
		currentBody = strings.TrimSpace(fmt.Sprintf("%s %s", currentBody, strings.TrimSpace(line)))
		continue
	sampling:
		if strings.HasPrefix(line, ">>> ") {
			if currentlySamplingBody != "" && currentlySamplingName != "" {
				currentSamples[currentlySamplingName] = currentlySamplingBody
				currentlySamplingBody = ""
			}
			currentlySamplingName = strings.TrimPrefix(line, ">>> ")
			continue
		}
		if strings.HasPrefix(line, ">>") && !strings.HasPrefix(line, ">>> ") {
			currentlySampling = false
			if currentlySamplingBody != "" && currentlySamplingName != "" {
				currentSamples[currentlySamplingName] = currentlySamplingBody
				currentlySamplingBody = ""
				currentlySamplingName = ""
			}
			ret.Elements = append(ret.Elements, Element{Samples: currentSamples})
			currentSamples = make(map[string]string)
		}
		if currentlySamplingName != "" {
			currentlySamplingBody = strings.TrimSpace(fmt.Sprintf("%s\n%s", currentlySamplingBody, line))
		}
	}
	if currentBody != "" {
		ret.Elements = append(ret.Elements, Element{Body: currentBody})
		currentBody = ""
	}
	currentBody = currentBody
	return ret
}

func main() {
	app := &cli.App{
		Name:  "galerie",
		Usage: "Turn Galerie files into QML code",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "standalone",
				Aliases: []string{"s"},
				Value:   false,
				Usage:   "Output a file that can be viewed standalone",
			},
		},
		Action: func(c *cli.Context) error {
			if c.Args().Len() < 1 {
				log.Fatalf("You need to provide an input file")
			}
			content, err := ioutil.ReadFile(c.Args().Get(0))
			standalone := c.Bool("standalone")
			if err != nil {
				log.Fatal(err)
			}
			text := string(content)
			doc := parse(text)
			fmt.Println("import QtQuick 2.12")
			fmt.Println("import QtQuick.Controls 2.12")
			fmt.Println("import QtQuick.Layouts 1.2")
			fmt.Println("import org.kde.kirigami 2.12 as Kirigami")
			if standalone {
				fmt.Println("Kirigami.ApplicationWindow {")
				fmt.Print("pageStack.initialPage: ")
			}
			fmt.Println("Kirigami.ScrollablePage {")
			fmt.Println("ColumnLayout {")
			fmt.Println("ColumnLayout {")
			fmt.Println(`Layout.maximumWidth: 800
Layout.alignment: Qt.AlignHCenter
spacing: 20`)
			for _, element := range doc.Elements {
				switch element.elmType() {
				case H1:
					fmt.Printf("Kirigami.Heading { text: \"%s\"; Layout.fillWidth: true }\n", element.H1)
				case H2:
					fmt.Printf("Kirigami.Heading { text: \"%s\"; level: 2; Layout.fillWidth: true }\n", element.H2)
				case Body:
					fmt.Printf("Label { text: \"%s\"; wrapMode: Text.WordWrap; Layout.fillWidth: true }\n", element.Body)
				case Samples:
					fmt.Printf("Frame { implicitWidth: 500; implicitHeight: 200; \n")
					fmt.Printf("Kirigami.SwipeNavigator { anchors.fill: parent; clip: true\n")
					for name, sample := range element.Samples {
						fmt.Printf(`Kirigami.Page {
title: "%s"
%s
}
`, name, sample)
					}
					fmt.Printf("}")
					fmt.Printf("}\n")
				}
			}
			fmt.Println("}")
			fmt.Println("}")
			fmt.Println("}")
			if standalone {
				fmt.Println("}")
			}
			return nil
		},
	}
	app.Run(os.Args)
}
