# Example Document
This is an example document showing you how to write a Galerie page.

## Interactive demo
This demo lets you preview Buttons.

>>
>>> Normal
Button {
    text: "Button"
}
>>> Flat
Button {
    text: "Button"
    flat: true
}
>>